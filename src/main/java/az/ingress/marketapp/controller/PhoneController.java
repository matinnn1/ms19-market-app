package az.ingress.marketapp.controller;

import az.ingress.marketapp.dto.request.PhoneRequestDto;
import az.ingress.marketapp.dto.response.MarketResponseDto;
import az.ingress.marketapp.dto.response.PhoneResponseDto;
import az.ingress.marketapp.genericsearch.SearchCriteria;
import az.ingress.marketapp.service.abstracts.PhoneService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class PhoneController {
    private final PhoneService phoneService;

    @PostMapping("/market/{marketId}/phone")
    public void createBrnachPhone(@PathVariable Long marketId, @RequestBody PhoneRequestDto phoneRequestDto){
        phoneService.create(marketId,phoneRequestDto);
    }
    @PostMapping("/market/{marketId}/branch/{branchId}/phone")
    public void createBrnachPhone(@PathVariable Long marketId, @PathVariable Long branchId, @RequestBody PhoneRequestDto phoneRequestDto){
        phoneService.create(marketId,branchId,phoneRequestDto);
    }

    @PostMapping("/market/{marketId}/branch/{branchId}/manager/{managerId}/phone")
    public void createBrnachPhone(@PathVariable Long marketId, @PathVariable Long branchId, @PathVariable Long managerId, @RequestBody PhoneRequestDto phoneRequestDto){
        phoneService.create(marketId,branchId,managerId,phoneRequestDto);
    }
}
