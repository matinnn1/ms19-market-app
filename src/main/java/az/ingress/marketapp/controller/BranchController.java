package az.ingress.marketapp.controller;

import az.ingress.marketapp.dto.request.BranchRequestDto;
import az.ingress.marketapp.dto.response.BranchResponseDto;
import az.ingress.marketapp.dto.response.MarketResponseDto;
import az.ingress.marketapp.genericsearch.SearchCriteria;
import az.ingress.marketapp.service.abstracts.BranchService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/market/{marketId}/branch")
@RequiredArgsConstructor
@Valid
public class BranchController {

    private final BranchService branchService;

    @PostMapping
    public BranchResponseDto create(@PathVariable Long marketId,
                                    @RequestBody BranchRequestDto branchRequestDto){

        return branchService.create(marketId, branchRequestDto);
    }

    @GetMapping
    public List<BranchResponseDto> getAll(@PathVariable Long marketId, Pageable pageable){
        return branchService.findAllByMarketId(marketId,pageable);
    }

    @GetMapping("/name")
    public List<BranchResponseDto> searchByName(@PathVariable Long marketId, @RequestParam String name){
        return branchService.findAllByName(marketId, name);
    }

    @GetMapping("/{branchId}")
    public BranchResponseDto get(@PathVariable Long marketId,@PathVariable Long branchId){
        return branchService.findByMarketIdAndBranchId(marketId,branchId);
    }

    @PutMapping("/{branchId}")
    public BranchResponseDto updateBranch(@PathVariable Long marketId, @PathVariable  Long branchId, @RequestBody BranchRequestDto branchRequestDto){
        return branchService.updateBranch(marketId,branchId,branchRequestDto);
    }

    @PostMapping("/search")
    public List<BranchResponseDto> search(@RequestBody List<SearchCriteria> searchCriteria) {
        return branchService.search(searchCriteria);
    }

    @DeleteMapping("/{branchId}")
    public void delete(@PathVariable Long marketId,Long branchId){
        branchService.delete(marketId,branchId);
    }
}
