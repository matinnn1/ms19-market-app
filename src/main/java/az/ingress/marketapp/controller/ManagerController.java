package az.ingress.marketapp.controller;

import az.ingress.marketapp.dto.request.ManagerRequestDto;
import az.ingress.marketapp.dto.response.BranchResponseDto;
import az.ingress.marketapp.dto.response.ManagerResponseDto;
import az.ingress.marketapp.genericsearch.SearchCriteria;
import az.ingress.marketapp.service.abstracts.ManagerService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/market/{marketId}/branch/{branchId}/manager")
@Valid
public class ManagerController {

    private final ManagerService managerService;

    @PostMapping
    public ManagerResponseDto create(@PathVariable Long marketId,
                                     @PathVariable Long branchId,
                                     @RequestBody ManagerRequestDto managerRequestDto) {
        var result = managerService.create(marketId, branchId, managerRequestDto);

        return result;
    }

    @GetMapping
    public List<ManagerResponseDto> getAll(@PathVariable Long marketId,
                                           @PathVariable Long branchId){
        return managerService.findAllByMarketIdAndBranchId(marketId, branchId);
    }

    @GetMapping("/{managerId}")
    public ManagerResponseDto getById(@PathVariable Long marketId,
                                      @PathVariable Long branchId,
                                      @PathVariable Long managerId){
        return managerService.findByMarketIdAndBranchIdAndManagerId(marketId, branchId, managerId);
    }

    @PutMapping("/{managerId}")
    public ManagerResponseDto update(@PathVariable Long marketId,
                                      @PathVariable Long branchId,
                                      @PathVariable Long managerId,
                                      @RequestBody ManagerRequestDto managerRequestDto){
        return managerService.update(marketId, branchId, managerId, managerRequestDto);
    }

    @PostMapping("/search")
    public List<ManagerResponseDto> search(@RequestBody List<SearchCriteria> searchCriteria) {
        return managerService.search(searchCriteria);
    }

    @DeleteMapping("/{managerId}")
    public void Delete(@PathVariable Long marketId,
                       @PathVariable Long branchId,
                       @PathVariable Long managerId){
        managerService.delete(marketId, branchId, managerId);
    }
}
