package az.ingress.marketapp.controller;

import az.ingress.marketapp.dto.request.MarketRequestDto;
import az.ingress.marketapp.dto.response.MarketResponseDto;
import az.ingress.marketapp.genericsearch.SearchCriteria;
import az.ingress.marketapp.service.abstracts.MarketService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/market")
@Valid
public class MarketController {

    private final MarketService marketService;

    @PostMapping
    public MarketResponseDto create(@RequestBody MarketRequestDto marketRequestDto) {
        return marketService.create(marketRequestDto);
    }

    @GetMapping
    public Page<MarketResponseDto> all(Pageable pageable) {
        return marketService.findAll(pageable);
    }

    @GetMapping("/{id}")
    public MarketResponseDto findById(@PathVariable Long id) {
        return marketService.findById(id);
    }

    @PutMapping("/{id}")
    public MarketResponseDto update(@PathVariable Long id, @RequestBody MarketRequestDto marketRequestDto) {
       return marketService.update(id, marketRequestDto);
    }

    @PostMapping("/search")
    public List<MarketResponseDto> search(@RequestBody List<SearchCriteria> searchCriteria) {
        return marketService.search(searchCriteria);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        marketService.delete(id);
    }
}
