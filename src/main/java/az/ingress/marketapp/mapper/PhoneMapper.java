package az.ingress.marketapp.mapper;

import az.ingress.marketapp.dto.request.PhoneRequestDto;
import az.ingress.marketapp.dto.response.PhoneResponseDto;
import az.ingress.marketapp.model.Market;
import az.ingress.marketapp.model.Phone;
import org.mapstruct.Mapper;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)

public interface PhoneMapper {
    Phone requestDtoToEntity(PhoneRequestDto phoneRequestDto);
    List<Phone> listRequestDtoToEntity(List<PhoneRequestDto> phoneRequestDtos);

    PhoneResponseDto entityToResponseDto(Phone phone);
    List<PhoneResponseDto> listEntityToResponseDto(List<Phone> phones);

}
