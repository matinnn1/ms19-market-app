package az.ingress.marketapp.mapper;


import az.ingress.marketapp.dto.request.MarketRequestDto;
import az.ingress.marketapp.dto.response.MarketResponseDto;
import az.ingress.marketapp.model.Market;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface MarketMapper {

    Market requestDtoToEntity(MarketRequestDto dto);

    @Mapping(source = "address", target = "marketAddress")
    List<MarketResponseDto> listEntityToResponseDto(List<Market> markets);

   // @Mapping(source = "address", target = "marketAddress")
   // Page<MarketResponseDto> pageEntityToResponseDto(Page<Market> markets);

    @Mapping(source = "address", target = "marketAddress")
    MarketResponseDto entityToResponseDto(Market market);

    @Mapping( source = "marketAddress", target = "address")
    Market responseDtoToEntity(MarketResponseDto dto);


}
