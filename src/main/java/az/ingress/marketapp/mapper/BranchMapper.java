package az.ingress.marketapp.mapper;


import az.ingress.marketapp.dto.request.BranchRequestDto;
import az.ingress.marketapp.dto.response.BranchResponseDto;
import az.ingress.marketapp.model.Branch;
import az.ingress.marketapp.model.Market;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface BranchMapper {

    Branch requestDtoToEntity(BranchRequestDto branchRequestDto);
    List<BranchResponseDto> listEntityToResponseDto(List<Branch> branches);

    //Page<BranchResponseDto> pageEntityToBranchResponseDto(Page<Branch> branches);

    BranchResponseDto entityToResponseDto(Branch branch);

}
