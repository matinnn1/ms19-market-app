package az.ingress.marketapp.mapper;


import az.ingress.marketapp.dto.request.ManagerRequestDto;
import az.ingress.marketapp.dto.response.ManagerResponseDto;
import az.ingress.marketapp.model.Manager;
import az.ingress.marketapp.model.Market;
import org.mapstruct.Mapper;

import java.util.List;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface ManagerMapper {

    Manager requestDtoToEntity(ManagerRequestDto requestDto);
    ManagerResponseDto entityToResponseDto(Manager manager);
    List<ManagerResponseDto> listEntityToResponseDto(List<Manager> managers);
}
