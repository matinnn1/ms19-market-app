package az.ingress.marketapp.service.abstracts;

import az.ingress.marketapp.customAnnotations.EntityField;
import az.ingress.marketapp.dto.response.ResponseDto;
import az.ingress.marketapp.genericsearch.SearchCriteria;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public interface SearchCriteriaService {
    default List<SearchCriteria> searchCriteriaFromDtoToEntity(List<SearchCriteria> searchCriteriaFromDto, ResponseDto responseDto) {
        List<SearchCriteria> searchCriteria = new ArrayList<>();

        Class<?> dtoClass = responseDto.getClass();

        Field[] dtoFields = dtoClass.getDeclaredFields();

        for (SearchCriteria searchCriterionFromDto : searchCriteriaFromDto) {
            for (Field dtoField : dtoFields) {
                SearchCriteria searchCriterion = new SearchCriteria();

                if (searchCriterionFromDto.getKey().equalsIgnoreCase(dtoField.getName())) {
                    searchCriterion.setValue(searchCriterionFromDto.getValue());
                    searchCriterion.setOperation(searchCriterionFromDto.getOperation());

                    if (dtoField.isAnnotationPresent(EntityField.class)) {
                        Annotation annotation = dtoField.getAnnotation(EntityField.class);

                        Class<? extends Annotation> annotationType = annotation.annotationType();

                        for (Method method : annotationType.getDeclaredMethods()) {
                            try {
                                Object entityFieldName = method.invoke(annotation);
                                searchCriterion.setKey(entityFieldName.toString());

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        searchCriterion.setKey(searchCriterionFromDto.getKey());
                    }

                    searchCriteria.add(searchCriterion);
                }
            }
        }
        return searchCriteria;
    }
}
