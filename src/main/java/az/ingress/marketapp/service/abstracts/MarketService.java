package az.ingress.marketapp.service.abstracts;

import az.ingress.marketapp.dto.request.MarketRequestDto;
import az.ingress.marketapp.dto.response.MarketResponseDto;
import az.ingress.marketapp.genericsearch.SearchCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MarketService {
    MarketResponseDto create(MarketRequestDto marketRequestDto);

    Page<MarketResponseDto> findAll(Pageable pageable);

    MarketResponseDto findById(Long id);

    MarketResponseDto update(Long id, MarketRequestDto marketRequestDto);

    List<MarketResponseDto> search(List<SearchCriteria> searchCriteria);

    void delete(Long id);
}
