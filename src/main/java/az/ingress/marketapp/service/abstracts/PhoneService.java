package az.ingress.marketapp.service.abstracts;

import az.ingress.marketapp.dto.request.PhoneRequestDto;
import az.ingress.marketapp.dto.response.PhoneResponseDto;
import az.ingress.marketapp.genericsearch.SearchCriteria;

import java.util.List;

public interface PhoneService {
    void create(Long marketId, PhoneRequestDto phoneRequestDto) ;
    void create(Long marketId, Long branchId, PhoneRequestDto phoneRequestDto);
    void create(Long marketId, Long branchId,Long managerId, PhoneRequestDto phoneRequestDto);
}
