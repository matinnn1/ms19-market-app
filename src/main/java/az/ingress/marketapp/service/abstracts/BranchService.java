package az.ingress.marketapp.service.abstracts;

import az.ingress.marketapp.dto.request.BranchRequestDto;
import az.ingress.marketapp.dto.response.BranchResponseDto;
import az.ingress.marketapp.genericsearch.SearchCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BranchService {
    BranchResponseDto create(Long marketId, BranchRequestDto branchRequestDto);
    List<BranchResponseDto> findAllByMarketId(Long marketId, Pageable pageable);
    BranchResponseDto findByMarketIdAndBranchId(Long marketId, Long branchId);
    List<BranchResponseDto> findAllByName(Long marketId, String name);

    BranchResponseDto updateBranch(Long marketId,Long id, BranchRequestDto branchRequestDto);
    void delete(Long marketId,Long id);

    List<BranchResponseDto> search(List<SearchCriteria> searchCriteria);
}
