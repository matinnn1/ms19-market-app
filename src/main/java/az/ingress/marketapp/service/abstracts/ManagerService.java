package az.ingress.marketapp.service.abstracts;

import az.ingress.marketapp.dto.request.ManagerRequestDto;
import az.ingress.marketapp.dto.response.ManagerResponseDto;
import az.ingress.marketapp.genericsearch.SearchCriteria;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface ManagerService {
    ManagerResponseDto create(Long marketId, Long branchId, ManagerRequestDto managerRequestDto);
    List<ManagerResponseDto> findAllByMarketIdAndBranchId(Long marketId, Long branchId);
    ManagerResponseDto findByMarketIdAndBranchIdAndManagerId(Long marketId,
                                Long branchId,
                                Long managerId);
    ManagerResponseDto update(Long marketId, Long branchId,Long id, ManagerRequestDto managerRequestDto);
    void delete(Long marketId, Long branchId,Long id);

    List<ManagerResponseDto> search(List<SearchCriteria> searchCriteria);
}
