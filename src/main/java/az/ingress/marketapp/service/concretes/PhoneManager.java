package az.ingress.marketapp.service.concretes;

import az.ingress.marketapp.dto.request.PhoneRequestDto;
import az.ingress.marketapp.dto.response.PhoneResponseDto;
import az.ingress.marketapp.genericsearch.CustomSpecification;
import az.ingress.marketapp.genericsearch.SearchCriteria;
import az.ingress.marketapp.mapper.PhoneMapper;
import az.ingress.marketapp.model.Branch;
import az.ingress.marketapp.model.Manager;
import az.ingress.marketapp.model.Market;
import az.ingress.marketapp.model.Phone;
import az.ingress.marketapp.repository.BranchRepository;
import az.ingress.marketapp.repository.ManagerRepository;
import az.ingress.marketapp.repository.MarketRepository;
import az.ingress.marketapp.service.abstracts.PhoneService;
import az.ingress.marketapp.service.abstracts.SearchCriteriaService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PhoneManager implements PhoneService , SearchCriteriaService {
    private final MarketRepository marketRepository;
    private final BranchRepository branchRepository;
    private final ManagerRepository managerRepository;
    private final PhoneMapper phoneMapper;

    @Override
    public void create(Long marketId,  PhoneRequestDto phoneRequestDto) {
        Market market = marketRepository.findById(marketId).orElseThrow(()->new RuntimeException("Market not found"));

        Phone phone = phoneMapper.requestDtoToEntity(phoneRequestDto);

        market.getPhones().add(phone);

        marketRepository.save(market);
    }

    @Override
    public void create(Long marketId, Long branchId, PhoneRequestDto phoneRequestDto) {
        Branch branch = branchRepository.findById(branchId).orElseThrow(()->new RuntimeException("Branch not found"));

        if(!branch.getMarket().getId().equals(marketId))
            throw new RuntimeException("Market id is not equals branch's market id");

        Phone phone = phoneMapper.requestDtoToEntity(phoneRequestDto);

        branch.getPhones().add(phone);

        branchRepository.save(branch);
    }

    @Override
    public void create(Long marketId, Long branchId,Long managerId, PhoneRequestDto phoneRequestDto) {
        Manager manager = managerRepository.findById(managerId).orElseThrow(()->new RuntimeException("Manager not found"));
        Branch branch = branchRepository.findById(branchId).orElseThrow(()->new RuntimeException("Branch not found"));

        if(!branch.getMarket().getId().equals(marketId))
            throw new RuntimeException("Market id is not equals branch's market id");

        if(!branch.getManager().getId().equals(managerId))
            throw new RuntimeException("Market id is not equals branch's market id");

        Phone phone = phoneMapper.requestDtoToEntity(phoneRequestDto);
        manager.getPhones().add(phone);
        branch.setManager(manager);
        branchRepository.save(branch);
    }

}
