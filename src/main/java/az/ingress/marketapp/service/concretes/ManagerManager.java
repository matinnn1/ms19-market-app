package az.ingress.marketapp.service.concretes;

import az.ingress.marketapp.dto.request.ManagerRequestDto;
import az.ingress.marketapp.dto.response.ManagerResponseDto;
import az.ingress.marketapp.dto.response.MarketResponseDto;
import az.ingress.marketapp.dto.response.ResponseDto;
import az.ingress.marketapp.genericsearch.CustomSpecification;
import az.ingress.marketapp.genericsearch.SearchCriteria;
import az.ingress.marketapp.mapper.ManagerMapper;
import az.ingress.marketapp.model.Branch;
import az.ingress.marketapp.model.Manager;
import az.ingress.marketapp.repository.BranchRepository;
import az.ingress.marketapp.repository.ManagerRepository;
import az.ingress.marketapp.service.abstracts.ManagerService;
import az.ingress.marketapp.service.abstracts.SearchCriteriaService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ManagerManager implements ManagerService , SearchCriteriaService {
    private final BranchRepository branchRepository;
    private final ManagerRepository managerRepository;
    private final ManagerMapper managerMapper;

    @Override
    public ManagerResponseDto create(Long marketId, Long branchId, ManagerRequestDto managerRequestDto) {
        var branch = branchRepository.findById(branchId).orElseThrow(RuntimeException::new);
        if (!branch.getMarket().getId().equals(marketId)) {
            throw new RuntimeException();
        }
        Manager manager = managerMapper.requestDtoToEntity(managerRequestDto);
        manager.setBranch(branch);
        return managerMapper.entityToResponseDto(managerRepository.save(manager));
    }

    @Override
    public List<ManagerResponseDto> findAllByMarketIdAndBranchId(Long marketId, Long branchId) {
        return managerMapper.listEntityToResponseDto(managerRepository.findAll());
    }

    @Override
    public ManagerResponseDto findByMarketIdAndBranchIdAndManagerId(Long marketId,
                                                                    Long branchId,
                                                                    Long managerId) {
        return managerMapper.entityToResponseDto(managerRepository.findById(managerId).orElse(null));
    }

    @Override
    public ManagerResponseDto update(Long marketId, Long branchId,Long id, ManagerRequestDto managerRequestDto) {
        var branch = branchRepository.findById(branchId).orElseThrow(RuntimeException::new);
        if (!branch.getMarket().getId().equals(marketId)) {
            throw new RuntimeException();
        }
        Manager manager = managerMapper.requestDtoToEntity(managerRequestDto);
        manager.setId(id);
        manager.setBranch(branch);
        return managerMapper.entityToResponseDto(managerRepository.save(manager));
    }

    @Override
    public void delete(Long marketId, Long branchId,Long id) {
        managerRepository.deleteById(id);
    }

    @Override
    public List<ManagerResponseDto> search(List<SearchCriteria> searchCriteria) {
        ResponseDto responseDto = new MarketResponseDto();

        List<SearchCriteria> searchCriteriaForEntity = searchCriteriaFromDtoToEntity(searchCriteria, responseDto);

        CustomSpecification<Manager> managerSpecification = new CustomSpecification<>(searchCriteriaForEntity);

        List<Manager> managers = managerRepository.findAll(managerSpecification);

        return managerMapper.listEntityToResponseDto(managers);
    }
}
