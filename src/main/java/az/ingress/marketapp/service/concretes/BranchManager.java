package az.ingress.marketapp.service.concretes;


import az.ingress.marketapp.dto.request.BranchRequestDto;
import az.ingress.marketapp.dto.response.BranchResponseDto;
import az.ingress.marketapp.dto.response.ResponseDto;
import az.ingress.marketapp.genericsearch.CustomSpecification;
import az.ingress.marketapp.genericsearch.SearchCriteria;
import az.ingress.marketapp.mapper.BranchMapper;
import az.ingress.marketapp.model.Branch;
import az.ingress.marketapp.model.Market;
import az.ingress.marketapp.repository.BranchRepository;
import az.ingress.marketapp.repository.MarketRepository;
import az.ingress.marketapp.service.abstracts.BranchService;
import az.ingress.marketapp.service.abstracts.SearchCriteriaService;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class BranchManager implements BranchService, SearchCriteriaService {

    private final BranchRepository branchRepository;
    private final MarketRepository marketRepository;
    private final BranchMapper branchMapper;
    @Override
    public BranchResponseDto create(Long marketId, BranchRequestDto branchRequestDto) {
        Market market = marketRepository.findById(marketId).orElseThrow(RuntimeException::new);
        Branch branch = branchMapper.requestDtoToEntity(branchRequestDto);
        branch.setMarket(market);
       return branchMapper.entityToResponseDto(branchRepository.save(branch));
    }

    @Override
    public BranchResponseDto findByMarketIdAndBranchId(Long marketId, Long branchId) {
        Branch branch = branchRepository.findById(branchId).orElse(new Branch());

        if(!branch.getMarket().getId().equals(marketId))
            throw new RuntimeException("Request martket id is not equals branch's market id");

        return branchMapper.entityToResponseDto(branch);
    }

    @Override
    public List<BranchResponseDto> findAllByName(Long marketId, String name) {

        Specification<Branch> branchSpecification = (root, query, criteriaBuilder)
                -> criteriaBuilder.like(root.get("name"),"%" + name + "%");

        var branches = branchRepository.findAll(branchSpecification);

        return  branchMapper.listEntityToResponseDto(branches);
    }

    @Override
    public BranchResponseDto updateBranch(Long marketId, Long branchId, BranchRequestDto branchRequestDto) {
        Branch branch = branchRepository.findById(branchId).orElseThrow(()->new RuntimeException("Branch not found"));

        if(!branch.getMarket().getId().equals(marketId))
            throw new RuntimeException("Request martket id is not equals branch's market id");

        branch=branchMapper.requestDtoToEntity(branchRequestDto);
        branch.setId(branchId);
        return branchMapper.entityToResponseDto(branchRepository.save(branch));
    }

    @Override
    public void delete(Long marketId,Long id) {
        Branch branch = branchRepository.findById(id).orElseThrow(()->new RuntimeException("Branch not found"));
        if(!branch.getMarket().getId().equals(marketId))
            throw new RuntimeException("Request martket id is not equals branch's market id");
        branchRepository.deleteById(id);
    }

    @Override
    public List<BranchResponseDto> search(List<SearchCriteria> searchCriteria) {

        ResponseDto responseDto = new BranchResponseDto();

        List<SearchCriteria> searchCriteriaForEntity = searchCriteriaFromDtoToEntity(searchCriteria, responseDto);

        CustomSpecification<Branch> branchSpecification = new CustomSpecification<>(searchCriteriaForEntity);

        List<Branch> branches = branchRepository.findAll(branchSpecification);

        return branchMapper.listEntityToResponseDto(branches);
    }


    @Override
    public List<BranchResponseDto> findAllByMarketId(Long marketId, Pageable pageable) {
        Page<Branch> branches = branchRepository.findAllByMarketId(marketId, pageable);


        //  return branchMapper.pageEntityToBranchResponseDto(branches);
        return new ArrayList<>();
    }
}
