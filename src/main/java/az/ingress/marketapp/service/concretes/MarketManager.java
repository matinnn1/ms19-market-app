package az.ingress.marketapp.service.concretes;

import az.ingress.marketapp.config.RedisConfig;
import az.ingress.marketapp.customAnnotations.EntityField;
import az.ingress.marketapp.dto.request.MarketRequestDto;
import az.ingress.marketapp.dto.response.MarketResponseDto;
import az.ingress.marketapp.dto.response.ResponseDto;
import az.ingress.marketapp.genericsearch.CustomSpecification;
import az.ingress.marketapp.genericsearch.SearchCriteria;
import az.ingress.marketapp.mapper.MarketMapper;
import az.ingress.marketapp.model.Market;
import az.ingress.marketapp.repository.MarketRepository;
import az.ingress.marketapp.service.abstracts.MarketService;
import az.ingress.marketapp.service.abstracts.SearchCriteriaService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.lang.reflect.Method;

@Service
@RequiredArgsConstructor
public class MarketManager implements MarketService , SearchCriteriaService {
    private final MarketRepository marketRepository;
    private final MarketMapper marketMapper;
    private final RedisConfig redisConfig;

    @Override
    public MarketResponseDto create(MarketRequestDto marketRequestDto) {
        Optional<Market> byName = marketRepository.findByName(marketRequestDto.getName());
        if (byName.isPresent()) {
            throw new RuntimeException();
        }
        Market market = marketMapper.requestDtoToEntity(marketRequestDto);
        return marketMapper.entityToResponseDto(marketRepository.save(market));
    }

    @Transactional(rollbackOn = Exception.class)
    @Override
    public Page<MarketResponseDto> findAll(Pageable pageable) {

        var marketList = marketRepository.findAll(pageable);

        List<MarketResponseDto> marketResponseDtos =
                marketList.getContent().stream().map(marketMapper::entityToResponseDto).toList();

        return new PageImpl<>(marketResponseDtos,marketList.getPageable(), marketList.getTotalElements());
    }

    @Override
    @Cacheable(key = "#id", cacheNames = "market")
    public MarketResponseDto findById(Long id) {
        return marketMapper.entityToResponseDto(marketRepository.findById(id).orElse(new Market()));
    }

    @Override
    @CachePut(key = "#id", cacheNames = "market")
    public MarketResponseDto update(Long id, MarketRequestDto marketRequestDto) {
        Market market = marketRepository.findById(id).orElseThrow(() -> new RuntimeException("Market not found"));

        market = marketMapper.requestDtoToEntity(marketRequestDto);
        market.setId(id);

        return marketMapper.entityToResponseDto(marketRepository.save(market));
    }

    @Override
    public List<MarketResponseDto> search(List<SearchCriteria> searchCriteria) {
        ResponseDto responseDto = new MarketResponseDto();

        List<SearchCriteria> searchCriteriaForEntity = searchCriteriaFromDtoToEntity(searchCriteria, responseDto);

        CustomSpecification<Market> marketSpecification = new CustomSpecification<>(searchCriteriaForEntity);

        List<Market> markets = marketRepository.findAll(marketSpecification);

        return marketMapper.listEntityToResponseDto(markets);
    }

    @Override
    @CacheEvict(key = "#id", cacheNames = "market")
    public void delete(Long id) {
        marketRepository.deleteById(id);
    }

}
