package az.ingress.marketapp.customAnnotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

@Retention(RetentionPolicy.RUNTIME) // Specifies when the annotation should be retained
@Target(ElementType.FIELD)
public @interface EntityField {
        String entityFieldName();
}
