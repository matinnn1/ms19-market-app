package az.ingress.marketapp.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Branch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    String address;
    Integer countOfEmployee;


    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @ToString.Exclude
    @JoinTable(
            name = "branch_phone",
            joinColumns = @JoinColumn(name = "branch_id"),
            inverseJoinColumns = @JoinColumn(name = "phone_id"))
    List<Phone> phones;

    @OneToOne(mappedBy = "branch")
    @JsonBackReference
    private Manager manager;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "market_id")
    @ToString.Exclude
    Market market;
}
