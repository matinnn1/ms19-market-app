package az.ingress.marketapp.enums;

public enum FilterType {
    EQUALS,
    START_WITH,
    END_WITH
}
