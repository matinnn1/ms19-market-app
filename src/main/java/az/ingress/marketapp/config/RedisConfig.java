package az.ingress.marketapp.config;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;

@Configuration
@ConfigurationProperties("redis")
@EnableConfigurationProperties(RedisConfig.class)
@RequiredArgsConstructor
@Data
public class RedisConfig {
    private String host;
    private Integer port;
    private String username;
    private String password;

    @Bean
    public JedisConnectionFactory jedisConnectionFactory(){
        var configuration = new RedisStandaloneConfiguration();
        configuration.setHostName(host);
        configuration.setPort(port);
        configuration.setUsername(username);
        configuration.setPassword(password);
        return  new JedisConnectionFactory(configuration);
    }
}
