package az.ingress.marketapp.repository;

import az.ingress.marketapp.model.Branch;
import az.ingress.marketapp.model.Manager;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface ManagerRepository extends JpaRepository<Manager, Long>, JpaSpecificationExecutor<Manager> {
    @Override
    @EntityGraph(attributePaths = "phones", type = EntityGraph.EntityGraphType.FETCH)
    Optional<Manager> findById(Long id);
}
