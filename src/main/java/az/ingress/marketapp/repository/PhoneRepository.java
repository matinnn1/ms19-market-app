package az.ingress.marketapp.repository;

import az.ingress.marketapp.model.Phone;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneRepository extends JpaRepository<Phone, Long> {
}
