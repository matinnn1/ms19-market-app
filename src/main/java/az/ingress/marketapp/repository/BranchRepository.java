package az.ingress.marketapp.repository;

import az.ingress.marketapp.model.Branch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface BranchRepository extends JpaRepository<Branch, Long> , JpaSpecificationExecutor<Branch> {

    @EntityGraph(attributePaths = "phones", type = EntityGraph.EntityGraphType.FETCH)
    Page<Branch> findAllByMarketId(Long marketId, Pageable pageable);

    @Query(value = "from Branch b join b.market m where m.id = :marketId and b.name LIKE %:name%", nativeQuery = false)
    Optional<List<Branch>> findAllByByName(Long marketId, String name);
}
