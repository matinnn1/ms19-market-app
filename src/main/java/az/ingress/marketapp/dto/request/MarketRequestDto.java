package az.ingress.marketapp.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class MarketRequestDto {
    @NotBlank
    @Size(min = 4, max = 30)
    String name;
    @NotBlank
    String address;
}
