package az.ingress.marketapp.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ManagerRequestDto {
    String name;
    String surname;
    Integer age;
    List<PhoneRequestDto> phones;
}
