package az.ingress.marketapp.dto.response;

import az.ingress.marketapp.customAnnotations.EntityField;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class MarketResponseDto  implements ResponseDto {
    public static final long serialVersionUID = 4328743342113L;

    Long id;
    @NotBlank
    @Size(min = 4, max = 30)
    String name;

    @EntityField(entityFieldName = "address")
    String marketAddress;
    List<BranchResponseDto> branches;
    List<PhoneResponseDto> phones;
}
