package az.ingress.marketapp.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ManagerResponseDto implements ResponseDto {

    Long id;
    String name;
    String surname;
    Integer age;
    List<PhoneResponseDto> phones;
}
