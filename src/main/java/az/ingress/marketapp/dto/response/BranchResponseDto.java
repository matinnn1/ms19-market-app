package az.ingress.marketapp.dto.response;

import az.ingress.marketapp.customAnnotations.EntityField;
import az.ingress.marketapp.dto.request.BranchRequestDto;
import az.ingress.marketapp.dto.request.PhoneRequestDto;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class BranchResponseDto implements ResponseDto {
    public static final long serialVersionUID = 4328743342113L;

    Long id;
    @NotBlank
    String name;
    @NotBlank
    String address;
    @Positive
    Integer countOfEmployee;
    List<PhoneRequestDto> phones;
    ManagerResponseDto manager;
}
